

(function ($, Drupal, drupalSettings) {
	Drupal.behaviors.APISireneDataFrBehavior = {
	  attach: function (context, settings) {
		var field_sirene_id = drupalSettings.api_sirene_open_data_france.field_sirene_id;
		var field_company_name_id = drupalSettings.api_sirene_open_data_france.field_company_name_id;
		var field_company_address_id = drupalSettings.api_sirene_open_data_france.field_company_address_id;
		var field_company_postal_code_id = drupalSettings.api_sirene_open_data_france.field_company_postal_code_id;
		var field_company_city_id = drupalSettings.api_sirene_open_data_france.field_company_city_id;

		$(document).ready(function() {

			//--> search by Siret ou Siren
			$('#'+field_sirene_id).autocomplete({
				source : function(requete, reponse){ // les deux arguments représentent les données nécessaires au plugin
				$.ajax({
						url : Drupal.url('api_sirene_open_data_france/entreprise-data-autocomplete'), // on appelle le script JSON
						dataType : 'json', // on spécifie bien que le type de données est en JSON
						type: "POST",
						data : {
							//variable envoyé avec la requête vers le serveur
							siret_sirene : $('#'+field_sirene_id).val(), // on donne la chaîne de caractère tapée dans le champ de recherche 
						},
						success : function(donnee){
							//donnee est la variable reçu du serveur avec les résultats
							reponse($.map(donnee, function(objet){
								console.log(donnee);
								var label = "";
								var value = "";
								var nom = "";
								var numero_voie = "";
								var type_voie = "";
								var libelle_voie = "";
								var code_postal = "";
								var libelle_commune = "";
								if(typeof objet.unite_legale === 'object' && objet.unite_legale != null) {
									//console.log("dans objet unite_legale");
									label = objet.unite_legale.denomination + " (" + objet.code_postal + ")";
									nom = objet.unite_legale.denomination;
									value = objet.siret;
									numero_voie = objet.numero_voie;
									type_voie = objet.type_voie;
									libelle_voie = objet.libelle_voie;
									code_postal = objet.code_postal;
									libelle_commune = objet.libelle_commune;
								}else{
									//console.log("else not in objet unite_legale");
									label = objet.denomination + " (" + objet.etablissement_siege.code_postal + ")";
									value = objet.siren;
									nom = objet.denomination;
									numero_voie = objet.etablissement_siege.numero_voie;
									type_voie = objet.etablissement_siege.type_voie;
									libelle_voie = objet.etablissement_siege.libelle_voie;
									code_postal = objet.etablissement_siege.code_postal;
									libelle_commune = objet.etablissement_siege.libelle_commune;
								}
								
								// on retourne cette forme de suggestion avec ces datas
								return {
									'label': label,
									'value': value,
									'nom': nom,
									'numero_voie': numero_voie,
									'type_voie': type_voie,
									'libelle_voie': libelle_voie, 
									'code_postal': code_postal,
									'libelle_commune': libelle_commune,
								}; 
							}));

						}
					});

				}
			});

			$('#'+field_sirene_id).on( "autocompleteselect", function( event, ui ) {
				var nom = ui.item.nom;
				var numero_voie = ui.item.numero_voie;
				var type_voie = ui.item.type_voie;
				var libelle_voie = ui.item.libelle_voie;
				var code_postal = ui.item.code_postal;
				var libelle_commune = ui.item.libelle_commune;

				$('#'+field_company_name_id).val(nom);
				$('#'+field_company_address_id).val(numero_voie + " " + type_voie + " " + libelle_voie);
				$('#'+field_company_postal_code_id).val(code_postal);
				$('#'+field_company_city_id).val(libelle_commune);
			});

			//--> search by full_text : nom de l'entreprise
			$('#'+field_company_name_id).autocomplete({
				source : function(requete, reponse){ // les deux arguments représentent les données nécessaires au plugin
				$.ajax({
						url : Drupal.url('api_sirene_open_data_france/entreprise-data-autocomplete'), // on appelle le script JSON
						dataType : 'json', // on spécifie bien que le type de données est en JSON
						type: "POST",
						data : {
							//variable envoyé avec la requête vers le serveur
							query_name : $('#'+field_company_name_id).val(), // on donne la chaîne de caractère tapée dans le champ de recherche 
						},
						success : function(donnee){
							//donnee est la variable reçu du serveur avec les résultats
							reponse($.map(donnee, function(objet){

								var label = "";
								var value = "";
								var type_voie = "";
								var numero_voie = "";
								var libelle_voie = "";
								var code_postal = "";
								var libelle_commune = "";
								
								label = objet.nom_raison_sociale + " (" + objet.code_postal + ")";
								value = objet.nom_raison_sociale;
								numero_voie = objet.numero_voie;
								type_voie = objet.type_voie;
								libelle_voie = objet.libelle_voie;
								code_postal = objet.code_postal;
								libelle_commune = objet.libelle_commune;
								
								// on retourne cette forme de suggestion avec ces datas
								return {
									'label': label,
									'value': value,
									'type_voie': type_voie,
									'numero_voie': numero_voie,
									'libelle_voie': libelle_voie, 
									'code_postal': code_postal,
									'libelle_commune': libelle_commune,
								}; 
							}));
						}
					});

				}
			});

			$('#'+field_company_name_id).on( "autocompleteselect", function( event, ui ) {
				var type_voie = ui.item.type_voie;
				var numero_voie = ui.item.numero_voie;
				var libelle_voie = ui.item.libelle_voie;
				var code_postal = ui.item.code_postal;
				var libelle_commune = ui.item.libelle_commune;
				$('#'+field_company_address_id).val(numero_voie + " " + type_voie + " " + libelle_voie);
				$('#'+field_company_postal_code_id).val(code_postal);
				$('#'+field_company_city_id).val(libelle_commune);
			});
		});
	}
  };
})(jQuery, Drupal, drupalSettings);