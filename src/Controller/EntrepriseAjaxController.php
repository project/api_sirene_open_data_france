<?php

namespace Drupal\api_sirene_open_data_france\Controller;

use Drupal\Core\Controller\ControllerBase;

use Drupal\Component\Serialization\JSON;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\api_sirene_open_data_france\EntrepriseDataApiManager;

class EntrepriseAjaxController extends ControllerBase {
  
  //function for autocomplete
  public function entreprise_data_autocomplete(Request $request){
    $return = []; //our variable to fill with data to return to autocomplete result
    
    $search = "sirene";
    $search_string = \Drupal::request()->request->get('siret_sirene');
    if(strlen($search_string) > 9) $search = "siret";
    
    if($search_string == "") {
      $search_string = \Drupal::request()->request->get('query_name');
      // $search = "full_text";
      $search="search";
    }
    //$type = "housenumber"; // can be housenumber, street, locality or municipality.

    $edataApi = new EntrepriseDataApiManager();
    switch($search){
      case 'sirene':
        $return = $edataApi->searchBySirene($search_string);
        break;
      case 'siret':
        $return = $edataApi->searchBySiret($search_string);
        break;
      case 'search':
        $search_data = [
          "q" => $search_string,
          "page" => 1,
          "per_page" => 10,
      ];
        $return = $edataApi->searchByName($search_data);
        foreach($return['results'] as $key=>$results){


        }
        // foreach($return['results'][0] as $key=>$val){
        //   \Drupal::logger('api_sirene_open_data_france')->error($key.'=>'.$val);
          foreach($return['results'] as $keyy=>$vall){

            foreach($vall['matching_etablissements'] as $keyyy=>$valll){
              foreach($valll as $keyyyy=>$vallll){
                if($keyyyy=='adresse') \Drupal::logger('api_sirene_open_data_france')->error($keyy.'.'.$keyyy.'.'.$keyyyy.'.'.$vallll);
                if($keyyyy=='siret') \Drupal::logger('api_sirene_open_data_france')->error($keyy.'.'.$keyyy.'.'.$keyyyy.'.'.$vallll);
          }
            }

          }

        // }
        // $return = $return['etablissement'];

        break;
    }

    return new JsonResponse(json_encode($return), 200, [], true);
  }
  
}