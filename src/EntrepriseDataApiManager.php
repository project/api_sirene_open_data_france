<?php

namespace Drupal\api_sirene_open_data_france;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Exception\RequestException;
/**
 * Basic manager of module.
 */
class EntrepriseDataApiManager {
  /**
   * The request url of the API.
   */
  // const API_URLv1 = 'https://entreprise.data.gouv.fr/api/sirene/v1';
  const API_URLv1 ='https://recherche-entreprises.api.gouv.fr';
  // const API_URLv3 = 'https://entreprise.data.gouv.fr/api/sirene/v3';
  const API_URLv3 = 'https://data.siren-api.fr/v3';
  
  public $client;
  /**
   * Constructor.
   */
  public function __construct() {
    if (!function_exists('curl_init')) {
      $msg = 'Entreprise Data API requires CURL module';
      \Drupal::logger('api_sirene_open_data_france')->error($msg);
      return;
    }
    $this->client = \Drupal::httpClient();
  }

  /**
   * Do CURL request with authorization.
   *
   * @param string $resource
   *   A request action of api.
   * @param string $method
   *   A method of curl request.
   * @param Array $inputs
   *   A data of curl request.
   *
   * @return array
   *   An associate array with respond data.
   */
  private function executeCurl($resource, $method, $inputs, $api) {
    if (!function_exists('curl_init')) {
      $msg = 'Entreprise Data API requires CURL module';
      \Drupal::logger('api_sirene_open_data_france')->error($msg);
      return NULL;
    }
    if(is_array($resource)){
      $api_url =  $api;
      foreach ($resource as $res){
        $api_url.="/".$res;
      }
    }else{
      $api_url = $api . "/" . $resource;
    }
    
    $options = [
      'headers' => [
        'Content-Type' => 'application/json'
      ],
    ];

    if (!empty($inputs)) {
      
      if($method == 'GET'){
        $api_url.= '?' . self::arrayKeyfirst($inputs) . '=' . array_shift($inputs);
        foreach($inputs as $param => $value){
            $api_url.= '&' . $param . '=' . $value;
        }
      }else{
        //POST request send data in array index form_params.
        //$options['body'] = $inputs;
      }
    }

    try {
      $clientRequest = $this->client->request($method, $api_url, $options);
      $body = $clientRequest->getBody();
    } catch (RequestException $e) {
      \Drupal::logger('api_sirene_open_data_france')->error('Curl error: @error', ['@error' => $e->getMessage()]);
    }

    return Json::decode($body);
  }

  /**
   * Get Request of API.
   *
   * @param string $resource
   *   A request action.
   * @param string $input
   *   A data of curl request.
   *
   * @return array
   *   A respond data.
   */
  public function curlGet($resource, $inputs, $api) {
    return $this->executeCurl($resource, "GET", $inputs, $api);
  }

  /**
   * Post Request of API.
   *
   * @param string $resource
   *   A request action.
   * @param string $inputs
   *   A data of curl request.
   *
   * @return array
   *   A respond data.
   */
  public function curlPost($resource, $inputs, $api) {
    return $this->executeCurl($resource, "POST", $inputs, $api);
  }

  public function searchBySiret($siret) {
    $resources = [
      "etablissements",
      $siret,
    ];
    return $this->curlGet($resources, [], self::API_URLv3);
  }

  public function searchBySirene($sirene) {
    $resources = [
      "unites_legales",
      $sirene,
    ];
    return $this->curlGet($resources, [], self::API_URLv3);
  }
  
  public function searchByName($name) {
    // $resources = [
    //   "full_text",
    //   $name,
    // ];
    $resources = [
      "search"
    ];

    return $this->curlGet($resources, $name, self::API_URLv1);
  }
  
  /**
   * Function to return first element of the array, compatability with PHP 5, note that array_key_first is only available for PHP > 7.3.
   *
   * @param array $array
   *   Associative array.
   *
   * @return string
   *   The first key data.
   */
  public static function arrayKeyfirst($array){
    if (!function_exists('array_key_first')) {
        foreach($array as $key => $unused) {
            return $key;
        }
        return NULL;
    }else{
        return array_key_first($array);
    }
  }

}
